package br.com.ozcorp;

/**
 * Enum de Referencia para os tipos sanguineo dos funcionarios
 * 
 * @author Guilherme Giroldo
 *
 */
public enum TipoSanguineo {

	A_POSITIVO("A+"), A_NEGATIVO("A-"), AB_POSITIVO("AB+"), B_POSITIVO("B+"), B_NEGATIVO("B-"), AB_NEGATIVO(
			"AB-"), O_POSITIVO("O+"), O_NEGATIVO("O-");

	public String tipo;

	TipoSanguineo(String tipo) {
		this.tipo = tipo;
	}
}
