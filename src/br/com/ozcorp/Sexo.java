package br.com.ozcorp;

/**
 * 
 * Enum de referencia para o Sexo Do Funcionario
 * 
 * @author Guilherme Giroldo
 *
 */
public enum Sexo {

	MASCULINO("Masculino"), FEMININO("Feminino"), OUTRO("Outro");

	public String nome;

	Sexo(String nome) {
		this.nome = nome;

	}

}
