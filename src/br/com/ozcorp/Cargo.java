package br.com.ozcorp;

/**
 * 
 * Classe para instanciar os atributos cargos do funcionarios
 * 
 * @author Guilherme Giroldo
 *
 */
public class Cargo {

	// Atributos
	private String titulo;
	private Double salarioBase;

	// Construtores
	public Cargo(String titulo, Double salarioBase) {
		super();
		this.titulo = titulo;
		this.salarioBase = salarioBase;
	}

	public Cargo() {
		// TODO Auto-generated constructor stub
	}

	// Getters N' Setters
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Double getSalarioBase() {
		return salarioBase;
	}

	public void setSalarioBase(Double salarioBase) {
		this.salarioBase = salarioBase;
	}

}
