package br.com.ozcorp;

/**
 * 
 * Classe criada apenas para realização teste de funcionalidade das demais
 * classes.
 * 
 * @author Guilherme Giroldo
 *
 */
public class FuncionarioTest {

	public static void main(String[] args) {

		// Funcionario teste = new Funcionario("Thomas Sowell", "123654789-12",
		// "987452178-31", "46548471",
		// "So-Well@email.com", "123654852", TipoSanguineo.AB_NEGATIVO, Sexo.MASCULINO,
		// 0,
		// new Departamento("Financeiro", "FNC", new Cargo("Diretor Financeiro",
		// 19500.89)));

		Funcionario teste2 = new Funcionario("Thomas Sowell", "123654789-12", "987452178-31", "46548471",
				"So-Well@email.com", "123654852", TipoSanguineo.AB_NEGATIVO, Sexo.MASCULINO, 0,
				new Departamento("Financeiro", "FNC", new Cargo("Diretor Financeiro", 19500.89)));

		// teste.Dados();

		Funcionario teste = new Diretor("Thomas Sowell", "123654789-12", "987452178-31", "46548471",
				"So-Well@email.com", "123654852", TipoSanguineo.AB_NEGATIVO, Sexo.MASCULINO, 0,
				new Departamento("Financeiro", "FNC", new Cargo("Diretor Financeiro", 19500.89)));
		
		System.out.println("\n" + "\n");
		teste.setCpf("555555555-555");

		teste.getDepartamento().getCargo().setSalarioBase(25000.00);

		// teste.Dados();

		System.out.println("Nome:               " + teste.getNome());
		System.out.println("RG:                 " + teste.getRg());
		System.out.println("CPF:                " + teste.getCpf());
		System.out.println("Matricula:          " + teste.getMatricula());
		System.out.println("E-mail:             " + teste.getEmail());
		System.out.println("Senha:              " + teste.getSenha());
		System.out.println("Cargo:              " + teste.getDepartamento().getCargo().getTitulo());
		System.out.println("Salario Base:       " + teste.getDepartamento().getCargo().getSalarioBase());
		System.out.println("Tipo Sanguineo:     " + teste.getTipoSanguineo());
		System.out.println("Sexo:               " + teste.getSexo());

		System.out.println("\n");

		teste2.Dados();

	}

}
