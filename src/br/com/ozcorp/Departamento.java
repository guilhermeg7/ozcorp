package br.com.ozcorp;

/**
 * 
 * Classe para instanciar os atributos do departamento
 * 
 * @author Guilherme Giroldo
 *
 */
public class Departamento extends Cargo {

	// Atributos
	private String nome;
	private String sigla;
	private Cargo cargo;

	// Construtores
	public Departamento(String nome, String sigla, Cargo cargo) {
		super();
		this.nome = nome;
		this.sigla = sigla;
		this.cargo = cargo;
	}

	public Departamento() {
		// TODO Auto-generated constructor stub
	}

	// Getters N' Setters
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

}
