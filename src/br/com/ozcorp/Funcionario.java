package br.com.ozcorp;

/**
 * Classe para instanciar os atributos pessoais dos funcionarios.
 * 
 * @author Guilherme Giroldo
 *
 * Date: 05/09/17
 */
public class Funcionario extends Departamento {

	// Atributos
	private String nome;
	private String rg;
	private String cpf;
	private String matricula;
	private String Email;
	private String senha;

	private TipoSanguineo tipoSanguineo;

	private Sexo sexo;

	private int nivelAcesso;

	private Departamento departamento;

	// Construtores
	public Funcionario(String nome, String rg, String cpf, String matricula, String email, String senha,
			TipoSanguineo tipoSanguineo, Sexo sexo, int nivelAcesso, Departamento departamento) {
		super();
		this.nome = nome;
		this.rg = rg;
		this.cpf = cpf;
		this.matricula = matricula;
		Email = email;
		this.senha = senha;
		this.tipoSanguineo = tipoSanguineo;
		this.sexo = sexo;
		this.nivelAcesso = nivelAcesso;
		this.departamento = departamento;
	}

	// Getters N' Setters
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public TipoSanguineo getTipoSanguineo() {
		return tipoSanguineo;
	}

	public void setTipoSanguineo(TipoSanguineo tipoSanguineo) {
		this.tipoSanguineo = tipoSanguineo;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public int getNivelAcesso() {
		return nivelAcesso;
	}

	public void setNivelAcesso(int nivelAcesso) {
		this.nivelAcesso = nivelAcesso;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	// metodos

public void Dados() {
		System.out.println("Nome:               " + getNome());
		System.out.println("RG:                 " + getRg());
		System.out.println("CPF:                " + getCpf());
		System.out.println("Matricula:          " + getMatricula());
		System.out.println("E-mail:             " + getEmail());
		System.out.println("Senha:              " + getSenha());
		System.out.println("Cargo:              " + getDepartamento().getCargo().getTitulo());
		System.out.println("Salario Base:       " + getDepartamento().getCargo().getSalarioBase());
		System.out.println("Tipo Sanguineo:     " + getTipoSanguineo());
		System.out.println("Sexo:               " + getSexo());
	}
}
